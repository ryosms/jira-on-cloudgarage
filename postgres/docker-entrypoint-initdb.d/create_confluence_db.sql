CREATE ROLE confluence_db_user WITH PASSWORD 'your-password' CREATEDB LOGIN;

CREATE DATABASE confluence_db WITH ENCODING 'utf-8' TEMPLATE template0 OWNER confluence_db_user;
